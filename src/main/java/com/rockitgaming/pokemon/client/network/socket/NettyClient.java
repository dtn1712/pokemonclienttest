package com.rockitgaming.pokemon.client.network.socket;

import com.rockitgaming.pokemon.network.util.ConverterUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author lamhm
 *
 */
public class NettyClient {
	private static final int PORT = 8080;
	private static BufferedReader console = null;


	public static void main(String[] args) {
		console = new BufferedReader(new InputStreamReader(System.in));
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			Bootstrap bootstrap = new Bootstrap();
			bootstrap.group(workerGroup);
			bootstrap.channel(NioSocketChannel.class);
			bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
			bootstrap.handler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					ch.pipeline().addLast(new ClientMessageHandler());
				}

			});

			ChannelFuture future = bootstrap.connect("localhost", PORT).sync();
			Channel channel = future.channel();
			while (true) {
				String readLine = console.readLine();
				byte[] convertInteger2Bytes = ConverterUtil.convertInteger2Bytes(Integer.parseInt(readLine));
				channel.writeAndFlush(Unpooled.wrappedBuffer(convertInteger2Bytes));
			}

			// chờ cho đến khi connection bị đóng
			// future.channel().closeFuture().sync();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// workerGroup.shutdownGracefully();
		}

	}

}
