package com.rockitgaming.pokemon.client.network.socket;

import com.rockitgaming.pokemon.client.network.CommandType;
import com.rockitgaming.pokemon.client.network.MapperUtils;
import com.rockitgaming.pokemon.client.network.codec.ClientMessageDecoder;
import com.rockitgaming.pokemon.client.network.codec.ClientMessageEncoder;
import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.util.IOUtils;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class TestClientSendMessage implements Runnable {

	private static final int PORT = 8080;
	private Socket socket = null;
	private Thread thread = null;
	private BufferedReader console = null;
	private DataOutputStream streamOut = null;
	private ChatClientThread client = null;


	public TestClientSendMessage() {
		System.out.println("Establishing connection. Please wait ...");
		try {
			socket = new Socket("localhost", PORT);
			System.out.println("Connected: " + socket);
			start();
		} catch (UnknownHostException uhe) {
			System.out.println("Host unknown: " + uhe.getMessage());
		} catch (IOException ioe) {
			System.out.println("Unexpected exception: " + ioe.getMessage());
		}
	}


	public void run() {
		while (thread != null) {
			try {
				String input = console.readLine();

				Map<String, String> messageData = new HashMap<>();
				messageData.put("username", "test1");
				messageData.put("channel", "test");
				messageData.put("messageContent", input);

				Message message = new Message();
				message.setCommandId(CommandType.COMMAND_SEND_MESSAGE);
				message.putString(Message.DATA_KEY, MapperUtils.getObjectMapper().writeValueAsString(messageData));
				sendMessage(message);
			} catch (Exception e) {
				System.out.println("Sending error: " + e.getMessage());
				stop();
			}
		}
	}


	public void sendMessage(Message message) {
		try {
			byte[] encode = ClientMessageEncoder.encode(message);
			System.out.println(Arrays.toString(encode));
			streamOut.write(encode);
			streamOut.flush();
		} catch (IOException ioe) {
			System.out.println("Sending error: " + ioe.getMessage());
			stop();
		}
	}

	public void receiveMessage(byte[] msg) {
		Message message = ClientMessageDecoder.decode(msg);
		System.out.println(message.getCommandId());
		System.out.println(message.getString(Message.DATA_KEY));
	}

	public void start() throws IOException {
		console = new BufferedReader(new InputStreamReader(System.in));
		streamOut = new DataOutputStream(socket.getOutputStream());
		if (thread == null) {
			client = new ChatClientThread(this, socket);
			thread = new Thread(this);
			thread.start();
		}
	}


	public void stop() {
		if (thread != null) {
			thread = null;
		}

		try {
			if (console != null)
				console.close();
			if (streamOut != null)
				streamOut.close();
			if (socket != null)
				socket.close();
		} catch (IOException ioe) {
			System.out.println("Error closing ...");
		}
		client.close();
	}


	public static void main(String args[]) {
		new TestClientSendMessage();
	}

	public class ChatClientThread extends Thread {
		private Socket socket = null;
		private TestClientSendMessage client = null;
		private InputStream streamIn = null;


		public ChatClientThread(TestClientSendMessage _client, Socket _socket) {
			client = _client;
			socket = _socket;
			open();
			start();
		}


		public void open() {
			try {
				streamIn = socket.getInputStream();
			} catch (IOException ioe) {
				System.out.println("Error getting input stream: " + ioe);
				client.stop();
			}
		}


		public void close() {
			try {
				if (streamIn != null)
					streamIn.close();
			} catch (IOException ioe) {
				System.out.println("Error closing input stream: " + ioe);
			}
		}

		public void run() {
			while (true) {

				try {
					int size = streamIn.available();
					if (size > 0) {
						System.out.println(Integer.toString(size));
						receiveMessage(IOUtils.toByteArray(streamIn));
					}
				} catch (IOException ioe) {
					System.out.println("Listening error: " + ioe.getMessage());
					client.stop();
				}
			}
		}
	}

}
