package com.rockitgaming.pokemon.client.network;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 1/7/17
 * Time: 5:49 PM
 */
public class TestEncryption {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.out.println(md5("apple"));
    }

    public static String md5(String input) throws NoSuchAlgorithmException {
        String result = input;
        if(input != null) {
            MessageDigest md = MessageDigest.getInstance("MD5"); //or "SHA-1"
            md.update(input.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            result = hash.toString(16);
            while(result.length() < 32) { //40 for SHA-1
                result = "0" + result;
            }
        }
        return result;
    }
}
