package com.rockitgaming.pokemon.client.network;


public class CommandType {

    public static final short COMMAND_REGISTER = 100;
    public static final short COMMAND_LOGIN = 101;
    public static final short COMMAND_LOGOUT = 102;

    public static final short COMMAND_SUBSCRIBE_CHANNEL = 100;
    public static final short COMMAND_SEND_MESSAGE = 102;



}
