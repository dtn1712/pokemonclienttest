package com.rockitgaming.pokemon.client.network.codec;

import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.util.ConverterUtil;

import java.util.Arrays;

/**
 * @author lamhm
 *
 */
public class ClientMessageDecoder {
	private static final int HEADER_LENGTH = 7;
	// max 0.5Mb
//	public static final int MAX_BODY_LENGTH = 512000;


	public static Message decode(byte[] data) {
		System.out.println(Arrays.toString(data));
		if (data.length < HEADER_LENGTH)
			throw new RuntimeException("Invalid data length");

		byte protocolVersion = data[0];
		short commandId = ConverterUtil.convertBytes2Short(data[1], data[2]);
		int bodyLength = ConverterUtil.convertBytes2Integer(data[3], data[4], data[5], data[6]);

		Message message = new Message();
		message.setProtocolVersion(protocolVersion);
		message.setCommandId(commandId);
		// trường hợp message quá lớn
//		if (bodyLength > MAX_BODY_LENGTH) {
//			throw new RuntimeException("Body length is too large");
//		}

		byte[] bodyData = new byte[bodyLength];
		// copy data từ vị trí nào của data copy vào body data vào index từ
		// 0->length
		System.arraycopy(data, HEADER_LENGTH, bodyData, 0, bodyLength);
		int i = 0;
		while (i < bodyLength) {
			try {
				// get key
				short key = ConverterUtil.convertBytes2Short(new byte[] { bodyData[i], bodyData[i + 1] }).shortValue();
				int valueLength = ConverterUtil.convertBytes2Integer(new byte[] { bodyData[(i + 2)], bodyData[(i + 3)], bodyData[(i + 4)], bodyData[(i + 5)] });

				if (valueLength > bodyLength) {
					throw new RuntimeException("Invalid data, invalid value length:" + valueLength + " for key " + key);
				}

				byte[] value = new byte[valueLength];
				System.arraycopy(bodyData, i + 6, value, 0, valueLength);
				message.putBytes(key, value);
				i += 6 + valueLength;
			} catch (Exception e) {
				throw new RuntimeException("Invalid data:" + e.getMessage());
			}
		}

		return message;
	}

}
