package com.rockitgaming.pokemon.client.network.socket;

import com.rockitgaming.pokemon.client.network.CommandType;
import com.rockitgaming.pokemon.client.network.MapperUtils;
import com.rockitgaming.pokemon.client.network.codec.ClientMessageDecoder;
import com.rockitgaming.pokemon.client.network.codec.ClientMessageEncoder;
import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.util.IOUtils;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;


public class TestClientAccount implements Runnable {

	private static final int PORT = 8080;
	private Socket socket = null;
	private Thread thread = null;
	private BufferedReader console = null;
	private DataOutputStream streamOut = null;
	private ChatClientThread client = null;


	public TestClientAccount() {
		System.out.println("Establishing connection. Please wait ...");
		try {
			socket = new Socket("localhost", PORT);
			System.out.println("Connected: " + socket);
			start();
		} catch (UnknownHostException uhe) {
			System.out.println("Host unknown: " + uhe.getMessage());
		} catch (IOException ioe) {
			System.out.println("Unexpected exception: " + ioe.getMessage());
		}
	}


	public void run() {
		while (thread != null) {
			try {
				String input = console.readLine();
				Message message = new Message();
				Map<String, String> messageData = new HashMap<>();
				if (input.equals("a")) {
					message.setCommandId(CommandType.COMMAND_REGISTER);
					messageData.put("username", "hello");
					messageData.put("password", "17121990ntd");
					messageData.put("email", "dtn1712@gmail.com");
				} else if (input.equals("b")){
					message.setCommandId(CommandType.COMMAND_LOGIN);
					messageData.put("username", "hello");
					messageData.put("password", "17121990ntd");
				} else if (input.equals("c")){
					message.setCommandId(CommandType.COMMAND_LOGOUT);
					messageData.put("username", "hello");
				}

				message.putString(Message.DATA_KEY, MapperUtils.getObjectMapper().writeValueAsString(messageData));
				sendMessage(message);
			} catch (Exception e) {
				System.out.println("Sending error: " + e.getMessage());
				stop();
			}
		}
	}


	public void sendMessage(Message message) {
		try {
			byte[] encode = ClientMessageEncoder.encode(message);
			streamOut.write(encode);
			streamOut.flush();
		} catch (IOException ioe) {
			System.out.println("Sending error: " + ioe.getMessage());
			stop();
		}
	}

	public void receiveMessage(byte[] msg) {
		Message message = ClientMessageDecoder.decode(msg);
		System.out.println(message.getCommandId());
		System.out.println(message.getString(Message.DATA_KEY));
//		Message message = ClientMessageDecoder.decode(msg);
//
//		System.out.println(message.getCommandId());
//		ObjectMapper objectMapper = MapperUtils.getObjectMapper();
//
//		try {
//			Account account = objectMapper.readValue(message.getString(Message.DATA_KEY),Account.class);
//			System.out.println(account);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	public void start() throws IOException {
		console = new BufferedReader(new InputStreamReader(System.in));
		streamOut = new DataOutputStream(socket.getOutputStream());
		if (thread == null) {
			client = new ChatClientThread(this, socket);
			thread = new Thread(this);
			thread.start();
		}
	}


	public void stop() {
		if (thread != null) {
			thread = null;
		}

		try {
			if (console != null)
				console.close();
			if (streamOut != null)
				streamOut.close();
			if (socket != null)
				socket.close();
		} catch (IOException ioe) {
			System.out.println("Error closing ...");
		}
		client.close();
	}


	public static void main(String args[]) {
		new TestClientAccount();
	}

	public class ChatClientThread extends Thread {
		private Socket socket = null;
		private TestClientAccount client = null;
		private InputStream streamIn = null;


		public ChatClientThread(TestClientAccount _client, Socket _socket) {
			client = _client;
			socket = _socket;
			open();
			start();
		}


		public void open() {
			try {
				streamIn = socket.getInputStream();
			} catch (IOException ioe) {
				System.out.println("Error getting input stream: " + ioe);
				client.stop();
			}
		}


		public void close() {
			try {
				if (streamIn != null)
					streamIn.close();
			} catch (IOException ioe) {
				System.out.println("Error closing input stream: " + ioe);
			}
		}

		public void run() {
			while (true) {

				try {
					int size = streamIn.available();
					if (size > 0) {
						System.out.println(Integer.toString(size));
						receiveMessage(IOUtils.toByteArray(streamIn));
					}
				} catch (IOException ioe) {
					System.out.println("Listening error: " + ioe.getMessage());
					client.stop();
				}
			}
		}
	}

}
